Soal 1 Buat DATABASE
//membuat DATABASE
CREATE DATABASE myshop;


Soal 2 Membuat table dalam DATABASE
//TABLE users
CREATE TABLE users(
    id INT AUTO_INCREMENT PRIMARY KEY, 
    name VARCHAR(255) NOT NULL, 
    email VARCHAR(255) NOT NULL, 
    password VARCHAR(255) NOT NULL
    );

//TABLE categories
CREATE TABLE categories(
    id INT AUTO_INCREMENT PRIMARY KEY, 
    name VARCHAR(255) NOT NULL
    );

//TABLE items
CREATE TABLE items(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL, 
    description VARCHAR(255), 
    price INT NOT NULL, 
    stock INT NOT NULL, 
    category_id INT, 
    FOREIGN KEY(category_id) REFERENCES categories(id)
    ); 


Soal 3 Memasukkan Data pada Table
//TABLE users
insert into users(name, email, password) values
("John Doe", "john@doe.com", "john123"), 
("Jane Doe", "jane@doe.com", "jane123");

//TABLE categories
insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

//TABLE items
insert into items(name, description, price, stock, category_id) values                                          ("Sumsang b50", "hape keren dari merk sumsang", 4000000, 100, 1),                                            -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),                                                -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


Soal 4 Mengambil Data dari Database
//soal A
select id, name, email from users;

//soal B
point 1
select * from items where price > 1000000;

point 2
select * from items where name like '%sang%';

//soal C
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori
from items
inner join categories on items.category_id = categories.id;


Soal 5 Mengubah Data dari Database
update items set price=2500000 where id=1;

